# VITS GST Emotion ID: Conditional Variational Autoencoder with Adversarial Learning for End-to-End Emotion Text-to-Speech 



This repo describes our approach to the first
task of the Text-to-speech challenge for VLSP
2022. For this task, teams are provided a speech
dataset that has several different emotional la-
bels. They are required to generate speech from
an input that consists of a text and an emotion.
Our approach includes a data pre-processing
pipeline and experiments on different model
architectures. A subjective human evaluation
is conducted on the results to show that our
system successfully learned to synthesise good
quality audios from a noisy and limited training
set.

## Pre-requisites
0. Python >= 3.6
0. Clone this repository
0. Install python requirements. Please refer [requirements.txt](requirements.txt)
    1. You may need to install espeak first: `apt-get install espeak`
0. Build Monotonic Alignment Search and run preprocessing if you use your own datasets.
```sh
# Cython-version Monotonoic Alignment Search
activate your virtual environment
cd monotonic_align
python setup.py build_ext --inplace

```

## Data
Each line in [filelists/new_train.txt.cleaned](filelists/new_train.txt.cleaned) and [filelists/new_val.txt.cleaned](filelists/new_val.txt.cleaned) is delimited by | into 3 fields
* file path
* emotion id
* text content
for example: 
path_to_audio/16199.wav|0|chúng ta đã cố thủ trong nhà an dưỡng
### Specification
* Audio File Type: wav
* Sample Rate: 22050 KHZ
* Number of train Audio Files task 2: 8000
* Number of validation Audio Files task 2: 273

data task 1 download [here](https://drive.google.com/drive/folders/1qmB-Ir1V_YprbNaT1aEo7DrMz6gKynxJ?usp=sharing)


## Training Example
Run this command
```sh
   sh train.sh
```
## Inference Example
See [inference_ms.py](inference_ms.py)
