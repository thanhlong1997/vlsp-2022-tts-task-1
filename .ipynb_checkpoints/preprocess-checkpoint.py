import argparse
import text
from utils import load_filepaths_and_text

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument("--out_extension", default="cleaned")
  parser.add_argument("--text_index", default=1, type=int)
  parser.add_argument("--filelists", nargs="+", default=["filelists/new_train.txt","filelists/new_val.txt"])
  parser.add_argument("--text_cleaners", nargs="+", default=["viet_cleaners"])
  # "filelists/metadata_train.csv",
  args = parser.parse_args()

  for filelist in args.filelists:
    lines = []
    print("START:", filelist)
    filepaths_and_text = load_filepaths_and_text(filelist)
    for i in range(len(filepaths_and_text)):
      original_text = filepaths_and_text[i][args.text_index]
     
      cleaned_text = text._clean_text(original_text, args.text_cleaners)
      
      filepaths_and_text[i][args.text_index] = cleaned_text
    new_filelist = filelist + "." + args.out_extension
    for i in filepaths_and_text:
      line = '/home/jovyan/vol-2/vlsp-2022/data_task_1/wavs/' + i[0] + '.wav' + '|' + i[1] + "|" + i[2]
      lines.append(line)
    with open(new_filelist, "w", encoding="utf-8") as f:
      for line in lines:
        f.write(line)
        f.write('\n')
