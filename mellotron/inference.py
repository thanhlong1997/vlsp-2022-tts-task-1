import numpy as np
from num2words import num2words
from  torch.cuda.amp import autocast
from scipy.io.wavfile import write
import sys
import unidecode
from unicodedata import normalize
from text_normalize.date_time import extract_time_pattent
from text_normalize.read_number import extract_number
from text_normalize.special_character import replace_special_character,replace_unit_count
from text_normalize.acronym_word import revert_acronym_word,english_acronym_word
from text_normalize.Foreign_language import convert_foreign_language,convert_foreign_language2
from text_normalize.all_entity_transform import read_senten_entity
from layers import TacotronSTFT
from text import  text_to_sequence,symbols
import librosa
import re
import torch
torch.cuda.set_device(2)
from collections import OrderedDict
from hparams import create_hparams
from model import Tacotron2, load_model

sys.path.append('waveglow/')
from waveglow.denoiser import Denoiser

import os
base = os.path.dirname(__file__)
hparams = create_hparams()
MAX_WAV_VALUE = hparams.max_wav_value

def load_mel(path):
    audio, sampling_rate = librosa.core.load(path, sr=hparams.sampling_rate)
    audio = torch.from_numpy(audio)
    if sampling_rate != hparams.sampling_rate:
        raise ValueError("{} SR doesn't match target {} SR".format(
            sampling_rate, stft.sampling_rate))
    audio_norm = audio.unsqueeze(0)
    audio_norm = torch.autograd.Variable(audio_norm, requires_grad=False)
    melspec = stft.mel_spectrogram(audio_norm)
    melspec = melspec.cuda()
    return melspec

stft = TacotronSTFT(hparams.filter_length, hparams.hop_length, hparams.win_length,
                    hparams.n_mel_channels, hparams.sampling_rate, hparams.mel_fmin,
                    hparams.mel_fmax)
# arpabet_dict = cmudict.CMUDict('data/cmu_dictionary')

### normalize text
def vi_num2words(num):
    return num2words(num, lang='vi')

def convert_time_to_text(time_string):
    try:
        h, m = time_string.split(":")
        time_string = vi_num2words(int(h)) + " giờ " + vi_num2words(int(m)) + " phút"
        return time_string
    except:
        return None

def replace_time(text):
    result = re.findall(r'\d{1,2}:\d{1,2}|', text)
    match_list = list(filter(lambda x: len(x), result))

    for match in match_list:
        if convert_time_to_text(match):
            text = text.replace(match, convert_time_to_text(match))
    return text

def replace_number(text):
    return re.sub('(?P<id>\d+)', lambda m: vi_num2words(int(m.group('id'))), text)

def split_segment(senten,max_len):
    segment = senten.split(',')
    result = [segment[0]]
    if len(segment) == 1:
        return result
    for index in range(1, len(segment)):
        if len(result[-1]) + len(segment[index]) > max_len:
            result.append(segment[index])
        else:
            result[-1]+=',' + segment[index]
    return result

def split_document(sentence,max_len):
    result = re.split('[.!?]',sentence)
    document = []
    for item in result:
        if len(item) > max_len:
            document.append(split_segment(item, max_len))
        else:
            document.append([item])
    return document

def replacenth(string, sub, wanted, n):
    where = [m.start() for m in re.finditer(sub, string)][n-1]
    before = string[:where]
    after = string[where:]
    after = after.replace(sub, wanted, 1)
    newString = before + after
    return newString

def normalize_text(text,do_normalize,type,max_len):
    # print(text)
    if do_normalize:
        text = re.sub('[:,;]\s', ' , ', text)
        text = revert_acronym_word(text)
        text = normalize("NFC", text)
        # text = text.strip(' ')
        text = re.sub('[!?]',' . ',text)
        text = re.sub('[\'"–\\\]',' ',text)
        text = re.sub('[;]',' , ',text)
        text = re.sub('\t',' ',text)
        while re.search('  ',text):
            text = re.sub('  ',' ',text)
        text = replace_special_character(text)
        text = replace_unit_count(text)
        text = extract_time_pattent(text)
        text = read_senten_entity(text,conference_threshold=0.75)
        text = extract_number(text)
        text = re.sub(',\s*\.\.\.',', ',text)
        while re.search('\.\.\.\s*\)',text):
            text = re.sub('\.\.\.\s*\)',')',text)

        while re.search('\.\.\.',text):
            text = re.sub('\.\.\.','.',text)

        while re.search('\n\n',text):
            text = re.sub('\n\n','\n',text)
        while re.search(',\s*,',text):
            text = re.sub(',\s*,',',',text)

        text = re.sub('\n', '. ', text)
        text = re.sub('\.\s*\.','. ',text)
        text2 = ''
        for char in list(text):
            if not re.search('[a-z]',unidecode.unidecode(char.lower())):
                if unidecode.unidecode(char) != char:
                    text2 += unidecode.unidecode(char)
                else:
                    # print(ord(unidecode.unidecode(char)),unidecode.unidecode(char) , ord(char),char)
                    text2 += char
            else:
                text2 += char

        text = text2
        text = re.sub('[:,;]\s', ' , ', text)
        if type ==1 :
            text = convert_foreign_language2(text)
        else:
            text = convert_foreign_language(text)
        text = re.sub('-', ' - ', text)
        text = read_senten_entity(text,conference_threshold=0.65)
        text = extract_number(text,last_call=True)
        text = re.sub('\)\s*,', ',', text)
        text = re.sub('\)\s*\.', '.', text)
        text = re.sub(',\(', ',', text)
        text = re.sub('\)', ',', text)
        text = re.sub('\(', ', ', text)
        text = re.sub('/', ' ', text)
        text = english_acronym_word(text)
        text = text.lower()
        while re.search('[^a-z*\s.,|\d]',unidecode.unidecode(text.lower())):
            replace = re.search('[^a-z*\s.,|\d]',unidecode.unidecode(text.lower()))
            start = replace.start()
            end = replace.end()
            text = text[:start] + ' '  + text[end:]
        list_sybole = list(symbols)
        list_sybole.extend(['|', '*'])
        text = ''.join(x for x in text if x in list_sybole)
        while re.search('  ',text):
            text = re.sub('  ',' ',text)
        while text[-1] in ['.','?','!',' ',',']:
            text = text[:-1]
            if len(text) == 0:
                break

    text = text.strip(' ')

    text = split_document(text,max_len)
    for index in range(len(text)):
        for index2 in range(len(text[index])):
            while re.search('  ', text[index][index2]):
                text[index][index2] = re.sub('  ', ' ', text[index][index2])
            text[index][index2] = re.sub(' ,', ',', text[index][index2])
    # return [text]
            text[index][index2] = re.sub(' \.', '.', text[index][index2])
            text[index][index2] = text[index][index2].strip(' ')
    return text

def init_model(tacotron_path,waveglow_path):
    model = load_model(hparams)
    state_dict = torch.load(tacotron_path,map_location='cpu')['state_dict']
    state_dict2 = OrderedDict()
    for key, value  in state_dict.items():
        state_dict2[key.replace('module.','')] = value
    model.load_state_dict(state_dict2)
    _ = model.cuda().eval().half()

    waveglow = torch.load(waveglow_path,map_location='cpu')['model'].cuda()
    waveglow.eval().half()
    for k in waveglow.convinv:
        k.float()
    denoiser = Denoiser(waveglow)
    return model,waveglow,denoiser

def waveglow_infer(mel_outputs_postnet,waveglow_model,denoiser):
    with torch.no_grad():
        audio = waveglow_model.infer(mel_outputs_postnet, sigma=0.666)
        audio = denoiser(audio, strength=0.01)[:, 0]
        audio = audio * hparams.max_wav_value
        audio = audio.squeeze()
        audio = audio.cpu().numpy()
    return audio

def tacotron_infer_senten(texts,speaker_id, style,tacotron_model,waveglow_model,denoiser):
    mel_outputs = []
    mel_output_postnets = []
    alignments = []
    print(texts)
    for index2 in range(len(texts)):
        print(texts[index2])
        text = texts[index2].lower()
        text_encoded = torch.LongTensor(text_to_sequence(text, hparams.text_cleaners))[None, :].cuda()
        # sequence = np.array(text_to_sequence(text, ['basic_cleaners'], punch=True))[None, :]
        # sequence = torch.autograd.Variable(
        #     torch.from_numpy(sequence)).cuda().long()
        speaker = torch.LongTensor([speaker_id]).cuda()
        print(text_encoded.size())
        print(speaker)
        if text_encoded.size()[1] == 0:
            continue
        start = time.time()
        mel_output, mel_output_postnet, _, alignment = tacotron_model.inference((text_encoded, style , speaker, None))
        print('mellotron inference time:',time.time() - start)
        print(mel_output_postnet.size())
        mel_outputs.append(mel_output)
        mel_output_postnets.append(mel_output_postnet)
        alignments.append(alignment)
    # all_mel_outputs[index] = mel_outputs
    # all_mel_output_postnets[index] = mel_output_postnets
    # all_alignments[index] = all_alignments
    audios = []
    if len(mel_output_postnets) == 0:
        return mel_outputs,np.zeros(0),alignments
    for index2 in range(len(mel_output_postnets)):
        start = time.time()
        audio = waveglow_infer(mel_output_postnets[index2], waveglow_model, denoiser)
        print('waveglow inference time:',time.time() - start)
        if index2 == 0:
            audios.append(audio)
        else:
            audios.append(np.asarray([0] * 7717))
            audios.append(audio)
    audios = np.concatenate(audios)
    return mel_outputs,audios,alignments

def inference_no_parallel(texts,speaker_id, style, tacotron_model,waveglow_model,denoiser):
    hparams.sampling_rate = 22050
    audios = []
    for index1 in range(len(texts)):
        # print(texts[index1])
        mel_outputs, audio, alignments = tacotron_infer_senten(texts[index1],speaker_id, style, tacotron_model, waveglow_model, denoiser)
        if type(audio) != int:
            if index1 != len(texts) - 1:
                audios.append(audio)
                audios.append(np.asarray([0] * 11025))
            else:
                audios.append(audio)
    audios = np.concatenate(audios)
    print(audios.shape)
    return audios

def pts_no_parallel(file_name,index,text, speaker_id, style, tacotron_model=None, waveglow_model=None, denoiser=None):
    # = init_model(tacortron_path,waveglow_path)
    import time
    start_time = time.time()
    text = normalize_text(text,do_normalize=True,type=1,max_len=150)
    audio1 = inference_no_parallel(text,speaker_id, style, tacotron_model, waveglow_model, denoiser)
    audio1 = audio1.astype('int16')
    audio_path1 = os.path.join(
        base , "wav/"+"{}_{}1.wav".format('output1', str(time.time())))
    print(audio_path1)
    print(audio1.shape)
    write(audio_path1, hparams.sampling_rate, audio1)
    file_name[index] = audio_path1
    # print('take : ', time.time() - start_time)
    return audio_path1

if __name__== '__main__':
    import time
    mel_spk1 = load_mel('/media/2T_SSD1/longlt/text_to_speech/data/phan4from2862.888to2877.108.wav')
    text = """Ngày 6/11/2021, Cơ quan Cảnh sát điều tra Bộ Công an đã ra các thủ tục tố tụng gồm Quyết định khởi tố bị can, Lệnh khám xét và Lệnh bắt bị can để tạm giam về tội, "Vi phạm quy định về đấu thầu gây hậu quả nghiêm trọng" theo Điều 222 Bộ luật Hình sự năm 2015, sửa đổi bổ sung năm 2017, đối với Nguyễn Minh Quân và Nguyễn Văn Lợi
            Sau khi Viện kiểm sát nhân dân tối cao phê chuẩn, Cơ quan Cảnh sát điều tra Bộ Công an đã thi hành các quyết định và lệnh đúng quy định pháp luật.
            Hiện Cơ quan Cảnh sát điều tra Bộ Công an đang tập trung lực lượng điều tra, làm rõ hành vi vi phạm của các bị can, mở rộng vụ án, triệt để thu hồi tài sản cho Nhà nước và người bệnh. """
    # text_encoded = torch.LongTensor(text_to_sequence(text, hparams.text_cleaners))[None, :].cuda()
    checkpoint_path = "/media/2T_SSD1/longlt/text_to_speech/mellotron/output_mellotron/best_model"
    waveglow_path = '/media/2T_SSD1/longlt/text_to_speech/model/waveglow_model/waveglow_14000'
    # waveglow_path = '/mnt/disk2/longlt/text_to_speech/mellotron/checkpoints/waveglow_20000'

    mel_spk2 = load_mel('/media/2T_SSD1/longlt/text_to_speech/data/TTS_01_000001.wav')
    mel_spk3 = load_mel('/media/2T_SSD1/longlt/text_to_speech/data/TTS_02_007030.wav')
    # sing_mel = load_mel('/media/2T_SSD1/longlt/text_to_speech/mellotron/data/sing.wav')
    # print(pitch_contour.size())
    # load source data to obtain rhythm using tacotron 2 as a forced aligner
    # x, y = mellotron.parse_batch(datacollate([dataloader[file_idx]]))
    speaker_id = 2
    tacotron_model, waveglow_model, denoiser = init_model(checkpoint_path, waveglow_path)
    start = time.time()
    pts_no_parallel([''], 0, text, speaker_id, mel_spk3, tacotron_model, waveglow_model, denoiser)
    print('take:', time.time() - start)
    # print(speaker_id)
    # speaker_id = torch.LongTensor([speaker_id]).cuda()
    # print(speaker_id)
    #
    # with torch.no_grad():
    #     with autocast():
    #         mel_outputs, mel_outputs_postnet2, gate_outputs, _ = mellotron.inference(
    #             (text_encoded, mel_spk2 , speaker_id, None))
    #
    #
    # with torch.no_grad():
    #     audio = denoiser(waveglow.infer(mel_outputs_postnet2, sigma=0.8), 0.01)[:, 0]
    # # audio = audio * MAX_WAV_VALUE
    # audio = audio.squeeze()
    # audio = audio.cpu().numpy()
    # write(os.path.join(base,'wav/test0.wav'), hparams.sampling_rate, audio)