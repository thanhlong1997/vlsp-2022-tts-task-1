import matplotlib.pyplot as plt

import os
import json
import math
import torch
from torch import nn
from torch.nn import functional as F
from torch.utils.data import DataLoader
import librosa

import commons
import utils
from data_utils import TextAudioLoader, TextAudioCollate, TextAudioSpeakerLoader, TextAudioSpeakerCollate
from models import SynthesizerTrn
from text.symbols import symbols
from text import text_to_sequence

from scipy.io.wavfile import write

# device = torch.device("cpu")
# model.to(device)
from mellotron.hparams import create_hparams
from mellotron.layers import TacotronSTFT
hparams = create_hparams()
stft = TacotronSTFT(hparams.filter_length, hparams.hop_length, hparams.win_length,
                    hparams.n_mel_channels, hparams.sampling_rate, hparams.mel_fmin,
                    hparams.mel_fmax)
def get_text(text, hps):
    text_norm = text_to_sequence(text.lower(), hps.data.text_cleaners)
    print("====", text_norm)
    if hps.data.add_blank:
        text_norm = commons.intersperse(text_norm, 0)
    print("++++", text_norm)
    text_norm = torch.LongTensor(text_norm)
    return text_norm

def load_mel(path):
    audio, sampling_rate = librosa.core.load(path, sr=hparams.sampling_rate)
    audio = torch.from_numpy(audio)
    if sampling_rate != hparams.sampling_rate:
        raise ValueError("{} SR doesn't match target {} SR".format(
            sampling_rate, stft.sampling_rate))
    audio_norm = audio.unsqueeze(0)
    audio_norm = torch.autograd.Variable(audio_norm, requires_grad=False)
    melspec = stft.mel_spectrogram(audio_norm)
    return melspec


hps = utils.get_hparams_from_file("./configs/vlsp_task1.json")

net_g = SynthesizerTrn(
    len(symbols),
    hps.data.filter_length // 2 + 1,
    hps.train.segment_size // hps.data.hop_length,
    n_speakers=hps.data.n_speakers,
    **hps.model).cuda()
_ = net_g.eval()

_ = utils.load_checkpoint("path_to_checkpoint/G_latest.pth", net_g, None)


reference_audios = {3:"/home/jovyan/vol-2/vlsp-2022/data_task_1/wavs/002795.wav", # angry
                   2:"/home/jovyan/vol-2/vlsp-2022/data_task_1/wavs/006332.wav", # happy
                   1:"/home/jovyan/vol-2/vlsp-2022/data_task_1/wavs/007230.wav", # sad
                   0:"/home/jovyan/vol-2/vlsp-2022/data_task_1/wavs/007045.wav" # neutral
                  }


stn_tst = get_text("dưới sự dẫn dắt của huấn luyện viên pác hang seo, Việt Nam đã duy trì vị thế là quốc gia đứng đầu khu vực Đông Nam Á, đồng thời cũng là quốc gia Đông Nam Á giữ vị trí lâu nhất trong tóp một trăm của phi pha.", hps)
import time
start = time.time()
emotion = 0
with torch.no_grad():
    x_tst = stn_tst.cuda().unsqueeze(0)
    x_tst_lengths = torch.LongTensor([stn_tst.size(0)]).cuda()
    emotion_id = torch.LongTensor([emotion]).cuda()
    reference_mel = load_mel(reference_audios[emotion]).cuda()
    audio = net_g.infer(x_tst, x_tst_lengths, emotion_id=emotion_id, mel=reference_mel, noise_scale=.667, noise_scale_w=0.8, length_scale=1)[0][0,0].data.cpu().float().numpy()

print("TIME: ", time.time()-start)
from  scipy.io import wavfile
sampling_rate = 22050
import soundfile as sf
sf.write('demo.wav', audio, sampling_rate)